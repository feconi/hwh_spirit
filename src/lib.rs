pub mod appointment {
    use std::collections::HashMap;
    use std::ops::Add;

    pub struct HAM {
        pub base: u64,
        pub step: u64,
    }
    
    pub const ADAMS: HAM        = HAM{base: 0, step: 1};
    pub const D_HONDT: HAM      = HAM{base: 1, step: 1};
    pub const SAINTE_LAGUE: HAM = HAM{base: 1, step: 2};
    pub const DANISH: HAM       = HAM{base: 1, step: 3};
    pub const IMPERIALI: HAM    = HAM{base: 2, step: 1};

    /* todo: non-ham algorithms
     * lr-droop
     * lr-hare
    */

    #[derive(Debug)]
    #[derive(Clone)]
    #[derive(PartialEq)]
    pub struct Parliament {
        pub factions: HashMap<String, u64>,
    }

    impl Parliament {
        pub fn appoint(&self, size: u64, method: &HAM) -> Parliament {

            let mut appointed = Parliament { factions: HashMap::new(), };

            // calc each first highest average
            let mut current_ham_values = HashMap::new();
            // max votes for base-zero hams to avoid division by zero
            let max_votes = self.factions
                .iter()
                .max_by(|a, b| a.1.partial_cmp(b.1).expect("Error: NaN"))
                .map(|(_k, v)| v)
                .expect("Error: No factions are given!");
            for faction in &self.factions {
                let quotient: f64 = match method.base {
                    0 => *faction.1 as f64 + *max_votes as f64,
                    _ => *faction.1 as f64 / method.base as f64,
                };
                current_ham_values.insert( faction.0.clone(), quotient );
            };

            // apportionment calculation by parliament seats
            for _seat in 0..size {
                // get max value faction key
                let f_key = current_ham_values
                    .iter()
                    .max_by(|a, b| a.1.partial_cmp(b.1).expect("Error: NaN"))
                    .map(|(k, _v)| k)
                    .expect("Error: No factions are given!")
                    .to_string();
                
                // give seat to found faction
                *appointed.factions.entry( f_key.clone() ).or_insert(0)+=1;

                // calc next ham value of used faction
                let divisor: u64= method.base + method.step * appointed.factions
                    .get(&f_key)
                    .copied()
                    .unwrap();
                let quotient: f64 = self.factions
                    .get(&f_key)
                    .copied()
                    .unwrap() as f64 / divisor as f64;  // divisor is not zero
                current_ham_values.insert(f_key, quotient);
            };
            appointed
        }
    }

    impl Parliament {
        pub fn electoral_threshold(&self, percentage: f64) -> Parliament {
            let mut cutted = self.clone();
            let total: u64 = cutted.factions.values().sum();
            cutted.factions.retain(|_, v| (*v * total) as f64 / 100.0 >= percentage );
            cutted
        }
    }

    impl Parliament {
        pub fn retain_by_seat(&self, count: u64, upper: bool) -> Parliament {
            let mut cutted = self.clone();
            match upper {
                true  => cutted.factions.retain(|_, v| *v >= count ),
                false => cutted.factions.retain(|_, v| *v <= count ),
            }
            cutted
        }
    }

    impl Parliament {
        pub fn overhang_seats(&self, constituencies: Parliament) -> Parliament {
            let mut overhanging = Parliament { factions: HashMap::new(), };
            for faction in &constituencies.factions {
                if let Some(x) = self.factions.get(faction.0) {
                    // parliament does contain constituency faction
                    // insert difference if constituency does not fit
                    if x < faction.1 {
                        overhanging.factions.insert(faction.0.to_owned(), *faction.1 - x);
                    }
                } else {
                    // parliament does not contain constituency faction
                    // insert overhang seat if it actually exist
                    if *faction.1 > 0 {
                        overhanging.factions.insert(faction.0.to_owned(), *faction.1);
                    }
                }
            }
            overhanging
        }
    }

    impl Add for Parliament {
        type Output = Self;
        fn add(self, other: Self) -> Self {
            let mut merged = self.clone();
            for faction in &other.factions {
                *merged.factions.entry( faction.0.to_owned() ).or_insert(0) += *faction.1;
            }
            merged
        }
    }
}


#[cfg(test)]
mod tests {
    use super::*;
    use appointment::*;
    use std::collections::HashMap;

    #[test]
    fn test_01() {
        let sl = HAM{ base:1, step:2 };
        let mut factions = HashMap::new();
        factions.insert(String::from("red"  ), 23);
        factions.insert(String::from("green"), 42);
        let p = Parliament{ factions };

        let mut result = HashMap::new();
        result.insert(String::from("red"  ), 2);
        result.insert(String::from("green"), 3);

        assert_eq!(result, p.appoint( 5, &sl ).factions);
    }

    #[test]
    fn test_02() {
        let mut factions = HashMap::new();
        factions.insert(String::from("blue"  ), 1754);
        factions.insert(String::from("brown" ),  366);
        factions.insert(String::from("pink"  ),  294);
        factions.insert(String::from("yellow"), 1527);
        factions.insert(String::from("red"   ),  422);
        factions.insert(String::from("purple"),  548);
        factions.insert(String::from("green" ),  341);
        factions.insert(String::from("black" ),   71);
        factions.insert(String::from("cyan"  ),  205);
        factions.insert(String::from("orange"),  265);
        factions.insert(String::from("white" ),   37);
        let p = Parliament { factions };

        let mut result = HashMap::new();
        result.insert(String::from("blue"  ), 11);
        result.insert(String::from("brown" ),  3);
        result.insert(String::from("pink"  ),  2);
        result.insert(String::from("yellow"),  9);
        result.insert(String::from("red"   ),  3);
        result.insert(String::from("purple"),  4);
        result.insert(String::from("green" ),  3);
        result.insert(String::from("black" ),  1);
        result.insert(String::from("cyan"  ),  2);
        result.insert(String::from("orange"),  2);
        result.insert(String::from("white" ),  1);
        let rp = Parliament { factions: result };

        assert_eq!(rp, p.appoint(41, &appointment::ADAMS));
    }

    #[test]
    fn test_03() {
        let mut factions = HashMap::new();
        factions.insert(String::from("red"  ), 23);
        factions.insert(String::from("green"), 42);
        let p = Parliament{ factions };

        let mut f2 = HashMap::new();
        f2.insert(String::from("red"  ), 7);
        f2.insert(String::from("green"), 8);
        let p2 = Parliament{ factions: f2 };

        let mut result = HashMap::new();
        result.insert(String::from("red"  ), 30);
        result.insert(String::from("green"), 50);
        let rp = Parliament{ factions: result };

        assert_eq!(rp, p + p2);
    }

    #[test]
    fn test_04() {
        let mut factions = HashMap::new();
        factions.insert(String::from("red"   ), 23);
        factions.insert(String::from("green" ), 42);
        factions.insert(String::from("blue"  ),  0);
        factions.insert(String::from("yellow"),  2);
        let p = Parliament{ factions };

        let mut result = HashMap::new();
        result.insert(String::from("red"  ), 23);
        result.insert(String::from("green"), 42);
        let rp = Parliament{ factions: result };

        assert_eq!(rp, p.electoral_threshold(5.0));
    }

    #[test]
    fn test_05() {
        let mut factions = HashMap::new();
        factions.insert(String::from("red"  ), 23);
        factions.insert(String::from("green"), 42);
        let p = Parliament{ factions };

        let mut f2 = HashMap::new();
        f2.insert(String::from("red" ), 42);
        f2.insert(String::from("blue"), 23);
        let p2 = Parliament{ factions: f2 };

        let mut result = HashMap::new();
        result.insert(String::from("red" ), 42-23);
        result.insert(String::from("blue"), 23);

        assert_eq!(result, p.overhang_seats(p2).factions);
    }

    #[test]
    fn test_06() {
        let mut factions = HashMap::new();
        factions.insert(String::from("red"  ), 23);
        factions.insert(String::from("green"), 42);
        factions.insert(String::from("blue"),   0);
        let p = Parliament{ factions };

        let mut result = HashMap::new();
        result.insert(String::from("red"  ), 23);
        result.insert(String::from("green"), 42);
        let rp = Parliament{ factions: result };

        let mut r2 = HashMap::new();
        r2.insert(String::from("red" ), 23);
        r2.insert(String::from("blue"),  0);
        let r2p = Parliament{ factions: r2 };

        assert_eq!(rp,  p.retain_by_seat(23, true ));
        assert_eq!(r2p, p.retain_by_seat(23, false));
    }
}
