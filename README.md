# hwh_spirit

A library for appointing parliament seats to parties according to a given election result.
This can be used for calculating real world parliaments (but not all) or in games that have some kind of elections.

![Logo](logo.png)

## toc

*todo*

## Installation

*todo*

## Usage

A basic usage to calculate an appointment looks like this:

```
use std::collections::HashMap;
use appointment::*;

let factions = HashMap::new();
factions.insert( String::from("red"  ), 23);
factions.insert( String::from("green"), 42);
factions.insert( String::from("blue" ), 75);

let votes = Parliament { factions };
let parliament_size = 31;

parliament = votes.appoint( parliament_size, &appointment::SAINTE_LAGUE );
```

Here we first create a HashMap containing each party as key and their election result as value.
Then we create a `Parliament` that just contains the HashMap `factions` we just created.
In the library there is no different data type for election result.
Instead an election result can be interpreted as a huge parliament that has as many seats as there are total votes.

Next we have to choose which method we want to use for appointing the parliament seats.
If you do not know what this is about, just use the Sainte-Laguë (aka. Webster) method as shown in the example.
For more information about *appointment algorithms* and *highest averages methods* look in wikipedia ([See related information](#related-information)) or some other ressources like your local election law.

Having created a `Parliament` (the election result) and an appointment method we can call the `appoint` function.
Its parameters are

1. `size`: the number of seats of the target parliament (`31` in the example),
2. `method`: the method how to appoint the parliament seats (`SAINTE_LAGUE` in the example).

This function returns a new `Parliament` containing a HashMap (`factions`) with the result.
It is the same data type we used for the election result.  

In this library there are some more functions for calculating on parliaments.

### Highest averages methods

The used highest averages method (HAM) in the example is Sainte-Laguë.
Sainte-Laguë is one of the fairest methods, but you may want to use a different one for some reason.
For example the adams method `let adams = HAM { base:0, step:1 };` ensures that every party gets one seat before any gets a second.
Or your election law defines some other.
There are many politic science papers out there comparing different methods.
On wikipedia you can read about many of them too ([see related information](#related-information)).  

The library includes the most common methods:

```
pub const ADAMS: HAM        = HAM{base: 0, step: 1};
pub const D_HONDT: HAM      = HAM{base: 1, step: 1};
pub const SAINTE_LAGUE: HAM = HAM{base: 1, step: 2};
pub const DANISH: HAM       = HAM{base: 1, step: 3};
pub const IMPERIALI: HAM    = HAM{base: 2, step: 1};
```

Maybe the method you want to use is not listed here.
It can be created like the others.

```
let my_silly_ham = HAM { base:3, step:1 };
```

The method implementation is written in integers (`u64`).
That means you may have to recalculate the method you want to use.
For example if you want to use the danish method, you can **not** create it like this:

```
let danish = HAM { base:0.333, step:1 }; // wrong number type
```

but like this:

```
let danish = HAM { base:1, step:3 };
```

Please note that you have to set the `step > 0`, else your appointment method will not work.
The `base` can be `0` as you can see in the `ADAMS` method.

## Implementation details

### About HAM

Almost every highest averages method can be identified by its

1. `base: u64` (first divisor),
2. `step: u64` (difference between each divisor) and
3. `divisor_function` (which is hard-coded as `base + step * k` for now).

More complex methods having a custom divisor function are not (yet) implemented.
So the highest averages method struct looks like this:

```
pub struct HAM {
    pub base: u64,
    pub step: u64,
}
```

Remember that the `step` must be `> 0` while the `base` can be `>= 0`!

#### Division by zero

In some HAMs (like Adams) a division by zero occures by definition.
These methods are defined in a way that each faction gets 1 seat before any faction gets their second.
So whenever a division by zero might occur, a proper maximal ham-value is calculated instead.
If there are more parties then parliament seats the library ensures that the parties gets seats in the correct order.

### Parliament Struct

The `Parliament` struct is the main struct of this library since it contains all needed data about election results or parliament appointments.
There is no difference how election results, parliament appointments or committee seats are stored and handled.
Each of them can be seen as a HashMap where the key is the factions or parties name and the value is the according number of votes or parliament/committees seats.
In the internal code of the library sometimes `seats` or called `votes`, `paties` are called `factions` or `constituencies` and vice versa.
Do not get confused by that.
Basically they are the same thing, just used in a different context.

That means all the data is stored in the Parliament struct:

```
pub struct Parliament {
    pub factions: HashMap<String, u64>,
}
```

You may have several partial elections in your election, like constituencies (*Wahlbezirke*) in germany.
If that is the case you may have to calculate partial parliaments and later add them together.
Check your local law how to proper calculate the parliament.
This library can not know about all of the different laws.
However, some basic math and functions are implemented in this library so you can build a software according to your specific law.
If you think this library should contain some new feature please make a feature request including an explanation why and how your local election law works.

### Parliament math

Included functions in this library are

* `pub fn appoint(&self, size: u64, method: &HAM) -> Parliament`
* `pub fn electoral_threshold(&self, percentage: f64) -> Parliament`
* `pub fn retain_by_seat(&self, count: u64, upper: bool) -> Parliament`
* `pub fn overhang_seats(&self, constituencies: Parliament) -> Parliament`
* `fn add(self, other: Self) -> Self`

All these functions return a new `Parliament`.
This is a design choice because in many election laws there are multiple appointments done from the same election result.
Just look at the *Overhang Seats* (*Überhangmandate*) in germany for an example.

Examples for these function will be in this README soon.
For now you could check the tests in the source code.

## Planned features

Since many election systems contain other weird stuff, these things will be available as features in this library (if I have heard about them).

## License

Unlicense.
See [LICENSE](LICENSE) for more information.

## Related information

Interesting pages on wikipadia related to this projects topic:

* [Apportionment in general](https://en.wikipedia.org/wiki/Apportionment_(politics))
* [Mathmatics of apportionment](https://en.wikipedia.org/wiki/Mathematics_of_apportionment)
* [Highest averages methods](https://en.wikipedia.org/wiki/Highest_averages_method)
* [Electoral threshold](https://en.wikipedia.org/wiki/Electoral_threshold)
